//tham khao bai lam cua thuc kate
import QtQuick 2.5
import QtQuick.Window 2.2

Window {
    visible: true
    width: 640
    height: 640
    title: qsTr("Image animation")
    id: root

   /* Rectangle{
        width: 100
        height: 100
        border.width: 1
        border.color: "yellow"

        Image{
            width: 99
            height: 99
            source: "/img/image/11.png"

            RotationAnimation on rotation {
                loops: Animation.Infinite
                from: 0
                to: 360
            }
        }
    } */

  /*  Rectangle{
        width: 100
        height: 100
        color: "blue"
        id: rect

        NumberAnimation on x{
         //   target: object
         //   property: "name"
         //   duration: 200
         //   easing.type: Easing.InOutQuad
            running: myHouse.pressed
            from: 0
            to: 500

        }

       MouseArea{
           id: myHouse
           anchors.fill: parent

           onClicked: {
               console.log(myHouse.x + ", " + myHouse.y)
           }
       }
    } */

    Rectangle{
        id: myRect
        width: 100
        height: 100
        x:40
        property variant listsourceimg: ["img/image/11.png","img/image/12.png","img/image/13.png"]
        property int index: 0
        property int repeat: 0

        Image {
            id: image
            source: "img/image/11.png"
            height: 100
            width: 60

            Timer{
                id: settime
                interval: 200; running: true; repeat: true;
                onTriggered:{
                    myRect.index++

                    if(myRect.index > 2){
                        myRect.index = 0
                    }
                    image.source = myRect.listsourceimg[myRect.index]
                }
            }

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if(myRect.repeat % 2 == 0)
                        settime.running = true
                    else
                        settime.running = false

                    myRect.repeat ++
                }
            }
        }

        states: [
            State{
                name: "downright"
                PropertyChanges{
                    target: myRect
                    x: myposition1.x - 100
                    y: myposition1.y - 100
                }
            },

            State{
                name: "downleft"
                PropertyChanges{
                    target: myRect
                  //  x: myposition2.x
                    y: myposition2.y - 100
                }
            },

            State{
                name: "upleft"
                PropertyChanges{
                    target: myRect
                    x: myposition3.x
                    y: myposition3.y + 40
                }
            }

        ]

        transitions: [

            Transition {
                from: "*"
                to: "downright"

                NumberAnimation{
                    properties: "x,y"
                    duration: 4000
                    easing.type: Easing.InOutQuad
                }
            },

            Transition {
                from: "*"
                to: "downleft"

                NumberAnimation{
                    properties: "x,y"
                    duration: 4000
                    easing.type: Easing.InOutQuad
                }
            },

            Transition {
                from: "*"
                to: "upleft"
                NumberAnimation{
                    properties: "x,y"
                    duration: 4000
                    easing.type: Easing.InOutQuad
                }
            },

            Transition {
                NumberAnimation{
                    properties: "x,y"
                    duration: 4000
                }
            }

        ]
    }

    Rectangle{//hinh chu nhat goc tren ben trai man hinh
        x: 0
        y: 0
        height: 40
        width: 40
        color: "blue"
        id: myposition3

        MouseArea{
            id: mouseArea
            anchors.fill: parent
            onClicked:{
                myRect.state == 'upleft' ? myRect.state = "" : myRect.state = 'upleft'
              //  console.log("ban dau(" + myposition3.x + ", " + myposition3.y+")")
            }
        }
    }

    Rectangle{//hinh chu nhat duoi ben phai man hinh
        x: 600
        y: 600
        height: 40
        width: 40
        color: "red"
        id: myposition1

        MouseArea{
            id: mouseArea2
            anchors.fill: parent
            onClicked:{
               myRect.state == 'downright' ? myRect.state = "" : myRect.state = 'downright'
            //     console.log("goc duoi (" + myposition1.x + ", " + myposition1.y+")")
            }
        }
    }

    Rectangle{//hinh chu nhat duoi ben trai man hinh
        x: 0
        y: 600
        height: 40
        width: 40
        color: "yellow"
        id: myposition2

        MouseArea{
            id: mouseArea3
            anchors.fill: parent
            onClicked:{
                myRect.state == 'downleft' ? myRect.state = "" : myRect.state = 'downleft'
              //  console.log("goc duoi trai (" + myposition2.x + ", " + myposition2.y+")")
            }
        }
    }

}
