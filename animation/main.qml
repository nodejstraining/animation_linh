import QtQuick 2.5
import QtQuick.Window 2.2

Window {
    visible: true
    width: 640
    height: 640

    Rectangle{
        x: 0
        y: 0
        height: 40
        width: 40
        color: "red"
        id: positionx

        MouseArea{
            anchors.fill: parent
            onClicked: {
                movetoposx.to = 40
                movetoposx.start()
                movetoposy.to = 40
                movetoposy.start()
            }
        }
    }

    Rectangle{
        x: 40
        y: 40
        property variant listsourceimg: ["img/image/11.png","img/image/12.png","img/image/13.png"]
        property int index: 0
        property int repeat: 0
        id: rect
        border.width: 2
        border.color: "blue"
        width: 105
        height: 105

        Image {//cho buc anh chuyen dong tai vi tri co dinh, lap lai cac anh nay
            id: image
            width: 100
            height: 100
            source: "img/image/11.png"

            Timer{
                id: settime
                interval: 250; running: true; repeat: true;
                onTriggered:{
                    rect.index++

                    if(rect.index > 2){
                        rect.index = 0
                    }

                    image.source = rect.listsourceimg[rect.index]
                }
            }

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if(rect.repeat % 2 == 0)
                        settime.running = true
                    else
                        settime.running = false

                    rect.repeat ++
                }
            }
        }

        PropertyAnimation on x{
            id: movetoposx
           // target: image
            to: 40
            duration: 2000
        }

        PropertyAnimation on y{
            id: movetoposy
         //   target: image
            to: 40
            duration: 2000
        }
    }


    Rectangle{
        x: 600
        height: 40
        width: 40
        color: "red"
        id: positiont

        MouseArea{
            anchors.fill: parent

            onClicked: {
                movetoposx.to = 500
                movetoposx.start()
                movetoposy.to = 40
                movetoposy.start()
            }
        }
    }



    Rectangle{
        x: 600
        y: 600
        height: 40
        width: 40
        color: "red"
        id: positiony

        MouseArea{
            anchors.fill: parent
            onClicked: {
                movetoposx.to = 500
                movetoposx.start()
                movetoposy.to = 500
                movetoposy.start()
            }
        }
    }

    Rectangle{
        x: 0
        y: 600
        height: 40
        width: 40
        color: "red"
        id: positionz

        MouseArea{
            anchors.fill: parent
            onClicked: {
                movetoposx.to = 40
                movetoposx.start()
                movetoposy.to = 500
                movetoposy.start()
            }
        }
    }

   /* Rectangle{
        id: hello
        width: 75
        height: 75
        color: "blue"
        opacity: 1.0

        MouseArea{
            anchors.fill: parent;
            onClicked: {
                hello1.start()
                hello2.start()
            }
        }

        PropertyAnimation{
            id: hello1
            target: hello; properties: "color"; to: "green"; duration: 5000
        }


        NumberAnimation {
            id: hello2
            targets: hello
            properties: "opacity"
            from: 0.99
            to: 1.0
            loops: Animation.Infinite
            easing {type: Easing.OutBack; overshoot: 500}
        }
    }

    Rectangle{
        id: helloooo
        width: 25
        height: 25
        color: "red"
        opacity: 1.0
        x: 500

        MouseArea{
            anchors.fill: parent;
            onClicked: {
                hello1.start()
                hello2.start()
            }
        }
    } */


   /* Rectangle{
        width: 100
        height: 100
        color: "red"


         SequentialAnimation on color{ //ParallelAnimation-chay song song

            ColorAnimation {
                to: "yellow"
                duration: 2000
            }

            ColorAnimation {
                to: "blue"
                duration: 2000
            }
        }
    } */

}
